# About

Following treats an adjustment of the SRTM DEM by using the Mean Dynamic Ocean
Topography tidal datum.

Input data:
*  SRTM DEM over Barbados
*  Mean Dynamic Ocean Topograohy model (MDOT)

Note, 

* the input SRTM digital elevation model is, originally,
referenced in the WGS84 gravimetric datum and covers the [Barbados] island.
* the MDOT is itself a global tidal vertical datum.

Output:
*  MDOT-adjusted SRTM DEM

The output DEM is referenced in the WGS84 ellipsoid.

# Proof of Concept

First, a GDAL-only dependent solution. The example treats the [Barbados] island
country, mapped inside the upper left and lower right corner coordinates

> Upper Left  ( -59.6599251,  13.3441563)
>
> Lower Right ( -59.4102029,  13.0355452)

[Barbados]: https://en.wikipedia.org/wiki/Barbados

```
gdal_calc.py \
-A srtm_brb.tif \
-B mean_dynamic_ocean_topography_brb.vrt \
-C srtm_brb_egm96.tif \
--outfile=srtm_brb_adjusted.tif \
--calc="(C==0)*0 + (C!=0)*(A-B/100)"
```

Decomposing this command into input and output blocks:

- `srtm_brb.tif` derives from `srtm_brb_egm96`
- `mean_dynamic_ocean_topography_brb.vrt` derives from
- `srtm_brb_egm96.tif` derives from 
- `srtm_brb_adjusted.tif` is obviously the ''output''

The ''calc'' formula:

- considers only non-zero values of the `srtm_brb_egm96.tif`
- subtracts the `mean_dynamic_ocean_topography_brb.vrt` from the `srtm_brb.tif`
- note, cm values of the `mean_dynamic_ocean_topography_brb.vrt` DEM are
divided by 100 to get converted in meters


The adjusted digital elevation model `srtm_brb_adjusted.tif`, pictured below,
is referenced in the WGS84 ellipsoid and has the following descriptive
statistics

> Minimum=-20.505, Maximum=330.042, Mean=82.799, StdDev=81.570

![Adjusted SRTM DEM over the Barbados island country][srtm_brb_adjusted_based_on_resampled_mdt]

[srtm_brb_adjusted_based_on_resampled_mdt]: srtm_brb_adjusted_based_on_resampled_mdt_50percent.png "Adjusted SRTM DEM over the Barbados island country"



and the following histogram
```
convert srtm_brb_adjusted.jpg -colorspace Gray -format %c histogram:info: |sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' |cut -d '(' -f1,3 |tr -d ' ' |tr -d '(' |tr -d ')' |asciigraph
```

```
                                                                      0  522999
                                                                      1  1761
                                                                      2  1516
                                                                      3  1144
█                                                                     4  647
█                                                                     5  529
█                                                                     6  249
█                                                                     7  208
██                                                                    8  61
██                                                                    9  15
██                                                                   10  8
██                                                                   11  2
███                                                                  12  5
██████████████████████████████████████████████████████████████      243  1
███████████████████████████████████████████████████████████████     244  2
███████████████████████████████████████████████████████████████     245  7
███████████████████████████████████████████████████████████████     246  27
███████████████████████████████████████████████████████████████     247  78
████████████████████████████████████████████████████████████████    248  188
████████████████████████████████████████████████████████████████    249  214
████████████████████████████████████████████████████████████████    250  474
████████████████████████████████████████████████████████████████    251  698
█████████████████████████████████████████████████████████████████   252  1056
█████████████████████████████████████████████████████████████████   253  1512
█████████████████████████████████████████████████████████████████   254  1955
██████████████████████████████████████████████████████████████████  255  463433
```

The source `srtm_brb.tif` elevation and the
`mean_dynamic_ocean_topography_brb.vrt` raster maps, referenced both in the
WGS84 ellipsoid, have respectively

> Minimum=-57.000, Maximum=294.000, Mean=46.620, StdDev=81.636

and

> Minimum=-37.103, Maximum=-34.990, Mean=-36.148, StdDev=0.485

## Vertical datum conversion

The SRTM DEM is referenced to a gravimetric vertical datum (WGS84/EGM96) while
the MDOT model is a tidal vertical datum itself. Referencing data sets to a
common vertical datum, is required to enable map algebra operations between
them.

Both data sets have been vertically shifted to the WGS84 ellipsoid. The
vertical shift is performed using `gdalwarp` along with the `gm96_15.gtx`
vertical datum definition:

For the SRTM DEM over [Barbados] which features, approximately, the following
histogram:
```
convert srtm_brb.jpg -colorspace Gray -format %c histogram:info: |sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' |cut -d '(' -f1,3 |tr -d ' ' |tr -d '(' |tr -d ')' |asciigraph
```

```
                                                                      0  495912
                                                                      1  28142
                                                                      2  1474
                                                                      3  1227
█                                                                     4  715
█                                                                     5  503
█                                                                     6  310
█                                                                     7  127
██                                                                    8  61
██                                                                    9  42
██                                                                   10  11
██                                                                   11  6
███                                                                  12  5
█████████████████████████████                                       114  1
█████████████████████████████                                       115  3
██████████████████████████████                                      116  5
██████████████████████████████                                      117  6
██████████████████████████████                                      118  10
██████████████████████████████                                      119  30
███████████████████████████████                                     120  71
███████████████████████████████                                     121  115
███████████████████████████████                                     122  234
███████████████████████████████                                     123  500
████████████████████████████████                                    124  724
████████████████████████████████                                    125  1071
████████████████████████████████                                    126  1286
████████████████████████████████                                    127  1737
█████████████████████████████████                                   128  458656
█████████████████████████████████                                   129  1869
█████████████████████████████████                                   130  1294
█████████████████████████████████                                   131  1071
██████████████████████████████████                                  132  605
██████████████████████████████████                                  133  469
██████████████████████████████████                                  134  261
██████████████████████████████████                                  135  83
███████████████████████████████████                                 136  66
███████████████████████████████████                                 137  29
███████████████████████████████████                                 138  34
███████████████████████████████████                                 139  3
████████████████████████████████████                                140  3
████████████████████████████████████████████████████████████████    251  1
█████████████████████████████████████████████████████████████████   252  3
█████████████████████████████████████████████████████████████████   253  3
█████████████████████████████████████████████████████████████████   254  1
██████████████████████████████████████████████████████████████████  255  10
```

shiffting the vertical datum via

```
gdalwarp srtm_egm96.tif srtm_brb.tif -s_srs "+proj=longlat +datum=WGS84 +no_defs +geoidgrids=egm96_15.gtx" -t_srs "+proj=longlat +datum=WGS84 +no_def"
```

derives, approximately, the following histogram:

```
convert srtm_brb_in_wgs84_ellipsoid.jpg -colorspace Gray -format %c histogram:info: |sed -e 's/^[[:space
:]]*//' -e 's/[[:space:]]*$//' |cut -d '(' -f1,3 |tr -d ' ' |tr -d '(' |tr -d ')' |asciigraph
```

```
                                                                      0  365758
                                                                      1  154993
                                                                      2  1565
                                                                      3  1210
█                                                                     4  698
█                                                                     5  495
█                                                                     6  293
█                                                                     7  119
██                                                                    8  61
██                                                                    9  42
██                                                                   10  11
██                                                                   11  6
███                                                                  12  5
█████████████████████████████                                       114  1
█████████████████████████████                                       115  3
██████████████████████████████                                      116  5
██████████████████████████████                                      117  6
██████████████████████████████                                      118  10
██████████████████████████████                                      119  30
███████████████████████████████                                     120  71
███████████████████████████████                                     121  115
███████████████████████████████                                     122  234
███████████████████████████████                                     123  500
████████████████████████████████                                    124  724
████████████████████████████████                                    125  1055
████████████████████████████████                                    126  1286
████████████████████████████████                                    127  1737
█████████████████████████████████                                   128  461962
█████████████████████████████████                                   129  1860
█████████████████████████████████                                   130  1294
█████████████████████████████████                                   131  1070
██████████████████████████████████                                  132  604
██████████████████████████████████                                  133  469
██████████████████████████████████                                  134  261
██████████████████████████████████                                  135  83
███████████████████████████████████                                 136  66
███████████████████████████████████                                 137  29
███████████████████████████████████                                 138  34
███████████████████████████████████                                 139  3
████████████████████████████████████                                140  3
████████████████████████████████████████████████████████████████    251  1
█████████████████████████████████████████████████████████████████   252  3
█████████████████████████████████████████████████████████████████   253  3
█████████████████████████████████████████████████████████████████   254  1
██████████████████████████████████████████████████████████████████  255  10
```

For the MDOT vertical datum:

```
gdalwarp mean_dynamic_ocean_topography_egm96.tif mean_dynamic_ocean_topography.tif -s_srs "+proj=longlat +datum=WGS84 +no_defs +geoidgrids=egm96_15.gtx" -t_srs "+proj=longlat +datum=WGS84 +no_def"

gdalbuildvrt mean_dynamic_ocean_topography_brb.vrt mean_dynamic_ocean_topography.tif -te -59.6599251 13.0355452 -59.4102029 13.3441563 -tr 0.000277777778000 0.000277777778000 -r bilinear
```

For the last raster translation, we need to define the extent of the output VRT map.


# Theoretical Background

## Basics

*  Types of vertical datums: tidal, gravimetric, geodetic
*  Obviously, if source is seal level -> tidal, if geoid -> gravimetric, if ellipsoid -> geodetic
*  MSL is a (type of a) vertical datum

## Question

Question asked on gdal-dev.

Of interest is the Mean Sea Level, based on NASA’s SRTM. I am new in this
topic. Is the following equation correct?

SRTM(MSL) = SRTM(WGS84) - MSL(WGS84) [*]


Is it "correct" to recommend the https://hydrosheds.cr.usgs.gov/ data
set for Mean Sea Level related studies?  I refer specifically to  i) 5.1
Void-filled digital elevation model  and  ii) 5.2 Hydrologically conditioned
elevation [**]


### From SRTM(EGM96) to SRTM(MHHW)

The report
<http://sealevel.climatecentral.org/uploads/research/Global-Mapping-Choices-Report.pdf>
describes the conversion of NASA’s SRTM, referenced to the EGM96 geoid, to
refer to the local mean higher-high water (MHHW) tidal datum, say SRTM(MHHW).


Notes

From https://icesat.gsfc.nasa.gov/icesat/tools/SRTM30_Documentation.html:
> “The geodetic reference for SRTM data is the WGS84 EGM96 geoid as documented at
http://www.nima.mil/GandG/wgsegm/, and no attempt was made to adjust the
vertical reference of either data set during the combination.”

From https://en.wikipedia.org/wiki/Shuttle_Radar_Topography_Mission:
> “The original SRTM elevations were calculated relative to the WGS84 ellipsoid
and then the EGM96 geoid separation values were added to convert to heights
relative to the geoid for all the released products.”

The conversion involves:

1. get MSL tidal, resolution. 2-arc-minute [0]

2. get MHHW(MSL), res. 2-arc-minute [1]

3. upsample MSL(TP) and MHHW(MSL) (based on NN) to SRTM's res. 3-arc-second [2]

4. geo-reference SRTM(EGM96) & MSL(TP) to WGS84; outputs: SRTM(WGS84) & MSL(WGS84) [3]

5. convert from "tidal" to "geodetic": MHHW(WGS84) = MHHW(MSL) + MSL(WGS84) | [4]

6. SRTM(MHHW) = SRTM(WGS84) - MHHW(WGS84) [5]


Details

[*] Equation re-built based on the steps described here

[**] chrome-extension://oemmndcbldboiebfnladdacbdfmadadm/https://hydrosheds.cr.usgs.gov/webappcontent/HydroSHEDS_TechDoc_v10.pdf

[0] begin with the global 2-arc-minute mean sea surface MSS_CNES_CLS_11
(Aviso 2014) based on 16 years of satellite altimetry observations
(1993-2009), referenced to the Topex-Poseidon ellipsoid, and here called
MSL(TP) (MSL for mean sea level);

[1] Employ a global MHHW grid, MHHW(MSL), referenced to the MSL tidal datum, provided by Mark Merrifield
of the University of Hawaii, and developed using the model TPXO8 at 2-arcminute resolution (Egbert et. al,
2002)

[2] Upsample these grids to 3-arc-second SRTM resolution and georeference using nearest-neighbor
interpolation

[3] Convert both SRTM(EGM96) and MSL(TP) to reference the WGS84 ellipsoid

[4] Convert MHHW grid to reference WGS84 through the operation, MHHWWGS84 = MSLWGS84 + MHHWMSL

[5] Convert SRTM to MHHW reference through the operation, SRTMMHHW = SRTMWGS84 - MHHWWGS84


## Test Conversion

Test conversion after answer in gdal-dev and updated answers in GIS.SE


### Theory

Ellipsoid Height = Orthometric Height + Geoid Height

See also:

*  https://vdatum.noaa.gov/images/docs/heights.gif
*  https://vdatum.noaa.gov/images/docs/clarke1866_wgs84_grs80.gif


### What we need

SRTM(MSL) which is an Orthometric Height. Thus,

Orthometric Height = Ellipsoid Height - Geoid Height


### What we have, based on the “Global Mapping Choices” report

*  SRTM(MSL) = SRTM(WGS84) - MSL(WGS84)

*  SRTM(WGS84) is geodetic, after the conversion SRTM(EGM96) → SRTM(WGS84).

Right? Or, is it ellipsoidal?

*  MSL(WGS84) is geodetic, after the conversion from tidal (TP) to geodetic
(WGS84)


### Test in Portugal

#### HydroSHEDS

*  n40w010_dem
*  n35w010_dem

#### SRTM

*  Projection  Geographic
*  Horizontal Datum  WGS84
*  Vertical Datum    EGM96 (Earth Gravitational Model 1996)
*  Vertical Units    Meters
*  Spatial Resolution   1 arc-second for global coverage (~30 meters), 3 arc-seconds for global coverage (~90 meters)
*  Raster Size    1 degree tiles
*  C-band Wavelength    5.6 cm


Sources

*  https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/

##### Test tiles

*  srtm_35_04 (n40w010_dem)
*  srtm_35_05 (n35w010_dem)


*  n37_w009_3arc_v2.tif


#### Conversion

```
gdalwarp n37_w009_3arc_v2.tif outdem.tif -s_srs EPSG:4326+5773 -t_srs EPSG:4979
```
*FAILS*:
```
ERROR 6: EPSG PCS/GCS code 4979 not found in EPSG support files.  Is this a valid
EPSG coordinate system?
ERROR 1: Translating source or target SRS failed:
EPSG:4979
```

```
gdalwarp n37_w009_3arc_v2.tif outdem.tif -s_srs EPSG:4326+5773 -t_srs "+proj=longlat +datum=WGS84 +no_def"
```
*WORKS*

```
gdalwarp n37_w009_3arc_v2.tif outdem2.tif -s_srs "+proj=longlat +datum=WGS84 +no_defs +geoidgrids=egm96_15.gtx" -t_srs "+proj=longlat +datum=WGS84 +no_def"
```
*WORKS*

```
gdalinfo n37_w009_3arc_v2.tif -nomd -hist
```
returns:
```
Driver: GTiff/GeoTIFF
Files: n37_w009_3arc_v2.tif
       n37_w009_3arc_v2.tif.aux.xml
Size is 1201, 1201
Coordinate System is:
GEOGCS["WGS 84",
    DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
        AUTHORITY["EPSG","6326"]],
    PRIMEM["Greenwich",0],
    UNIT["degree",0.0174532925199433],
    AUTHORITY["EPSG","4326"]]
Origin = (-9.000416666666666,38.000416666666666)
Pixel Size = (0.000833333333333,-0.000833333333333)
Corner Coordinates:
Upper Left  (  -9.0004167,  38.0004167) (  9d 0' 1.50"W, 38d 0' 1.50"N)
Lower Left  (  -9.0004167,  36.9995833) (  9d 0' 1.50"W, 36d59'58.50"N)
Upper Right (  -7.9995833,  38.0004167) (  7d59'58.50"W, 38d 0' 1.50"N)
Lower Right (  -7.9995833,  36.9995833) (  7d59'58.50"W, 36d59'58.50"N)
Center      (  -8.5000000,  37.5000000) (  8d30' 0.00"W, 37d30' 0.00"N)
Band 1 Block=1201x3 Type=Int16, ColorInterp=Gray
  Min=-13.000 Max=896.000
  Minimum=-13.000, Maximum=896.000, Mean=122.938, StdDev=110.742
  256 buckets from -14.7824 to 897.782:
  2 5 28 1519 337743 4183 5161 4059 5725 4796 7172 5881 8793 10218 8000 11633 9848 14400 11349 16460 18212 14305 20227 15649 20656 15795 20536 20151 15469 21056 16601 21597 15471 18935 13585 18014 17503 13174 17424 14491 19005 13612 17921 17628 13321 17984 13127 17168 12662 16562 16191 12196 16318 12370 16274 12161 15908 11542 15251 14491 10336 13448 9862 12431 8954 11830 11478 8327 10773 7880 9998 7348 9825 9592 6912 8737 6267 7808 5790 7274 5248 6802 6389 4520 5575 4190 5118 3754 4757 4350 3062 3981 2717 3664 2673 3388 2441 3147 2916 2160 2712 1956 2453 1842 2244 2221 1556 1998 1412 1932 1344 1763 1730 1248 1563 1165 1472 1013 1347 1056 1319 1282 888 1218 937 1197 818 1110 1116 794 995 659 906 608 796 726 518 628 404 536 354 434 290 333 297 201 276 167 207 152 196 174 138 176 99 139 95 133 121 81 107 62 95 50 89 57 70 81 55 79 57 68 47 77 70 55 92 46 66 39 68 55 65 57 43 67 45 53 48 60 55 32 52 35 54 35 45 45 43 57 46 44 33 44 42 46 48 25 43 28 27 34 21 25 27 27 29 21 24 43 21 20 33 20 26 25 34 22 26 32 21 27 16 29 9 24 12 8 12 6 15 9 5 16 5 12 10 4 9 11 8 15 8 6 4 3
  NoData Value=-32767
  Unit Type: m
```

and

```
gdalinfo n37_w009_3arc_v2_in_wgs84_ellipsoid.tif -nomd -hist
```
returns:
```
Driver: GTiff/GeoTIFF
Files: n37_w009_3arc_v2_in_wgs84_ellipsoid.tif
       n37_w009_3arc_v2_in_wgs84_ellipsoid.tif.aux.xml
Size is 1201, 1201
Coordinate System is:
GEOGCS["WGS 84",
    DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
        AUTHORITY["EPSG","6326"]],
    PRIMEM["Greenwich",0],
    UNIT["degree",0.0174532925199433],
    AUTHORITY["EPSG","4326"]]
Origin = (-9.000416666666666,38.000416666666659)
Pixel Size = (0.000833333333333,-0.000833333333333)
Corner Coordinates:
Upper Left  (  -9.0004167,  38.0004167) (  9d 0' 1.50"W, 38d 0' 1.50"N)
Lower Left  (  -9.0004167,  36.9995833) (  9d 0' 1.50"W, 36d59'58.50"N)
Upper Right (  -7.9995833,  38.0004167) (  7d59'58.50"W, 38d 0' 1.50"N)
Lower Right (  -7.9995833,  36.9995833) (  7d59'58.50"W, 36d59'58.50"N)
Center      (  -8.5000000,  37.5000000) (  8d30' 0.00"W, 37d30' 0.00"N)
Band 1 Block=1201x3 Type=Int16, ColorInterp=Gray
  Min=-5.000 Max=658.000
  Minimum=-5.000, Maximum=658.000, Mean=123.931, StdDev=109.171
  256 buckets from -6.3 to 659.3:
  94 644 335188 4881 2673 3777 2720 4082 4366 3141 5142 3685 5881 6543 4685 7783 5259 8355 9215 6652 10764 7310 11640 12495 8958 14033 9526 15123 15539 10356 15514 10535 15524 15109 10140 15586 10160 15953 16601 10903 15850 10315 14361 13708 8964 13501 8821 13140 13177 8598 12848 11219 13794 13612 9091 13174 8801 13276 13429 9083 13127 8594 12810 12543 8203 12427 8006 12196 12215 8218 12332 8109 12181 12072 7904 11542 7643 11263 10836 6946 10195 6643 9862 9363 6146 8902 5914 8761 8423 5511 8153 5295 7729 7474 4957 7300 4916 7200 7066 4427 6548 4226 6015 5739 3885 5455 3567 5251 5051 3197 4738 2974 4193 4185 2663 3842 2528 3671 3519 2110 3113 2025 2938 2717 1838 2768 1731 2531 2486 1631 2328 1451 2217 2096 1364 1949 1260 1860 1842 1135 1674 1121 1590 1519 980 1412 987 1378 1386 867 1306 845 1248 1158 799 1122 741 1065 1000 675 1056 664 995 942 574 905 627 937 919 536 859 538 874 784 525 745 486 653 676 422 583 399 554 518 336 464 277 410 357 250 335 194 273 255 151 226 121 209 174 109 158 106 150 137 81 133 96 133 99 67 106 61 100 90 64 81 52 76 76 41 52 38 68 57 37 53 40 58 58 39 57 35 46 55 39 51 36 55 71 33 51 32 47 45 32 55 34 53 35 26 51 33 45 40 29 1785
  NoData Value=-32767
  Unit Type: m
```

# Links

## Theoretical Background

### Definitions

* https://vdatum.noaa.gov/docs/datums.html
* http://www.altimetry.info/radar-altimetry-tutorial/data-flow/data-processing/reference-surfaces/
* https://gis.stackexchange.com/questions/75572/how-is-elevation-and-altitude-measured

### Mean Sea Level

* http://www.esri.com/news/arcuser/0703/geoid1of3.html
* https://gis.stackexchange.com/questions/212911/dem-showing-areas-in-the-ocean-to-be-greater-than-0m-my-msl-in-my-project/213037#213037
* https://gis.stackexchange.com/questions/206076/is-elevation-height-relative-to-sea-level-constant-despite-sea-level-rising/206129#206129

### Harmonic analysis

* https://tidesandcurrents.noaa.gov/harmonic.html

### Geoid

* http://georepository.com/datum_5171/EGM96-geoid.html
* https://www.ngs.noaa.gov/GEOID/
* http://www.space.dtu.dk/english/Research/Scientific_data_and_models/downloaddata

### Vertical Datum Transformations

* https://gis.stackexchange.com/questions/24023/how-are-egm96-and-wgs84-related-to-each-other
* https://gis.stackexchange.com/questions/664/whats-the-difference-between-a-projection-and-a-datum
* https://gis.stackexchange.com/questions/221258/vertical-datum-and-projection?rq=1
* https://gis.stackexchange.com/questions/230029/when-is-data-created-in-datums-other-than-wgs84?rq=1

### Mean Dynamic Topography

* https://www.aviso.altimetry.fr/en/applications/ocean/large-scale-circulation/mean-dynamic-topography.html
* https://www.aviso.altimetry.fr/en/data/products/auxiliary-products/mdt.html
* https://www.aviso.altimetry.fr/en/data/products/auxiliary-products/mdt/mdt-description.html
* https://www.aviso.altimetry.fr/fileadmin/documents/OSTST/2013/oral/mulet_MDT_CNES_CLS13.pdf
> PDF file

* http://journals.ametsoc.org/doi/abs/10.1175/2008JTECHO568.1
* DOI: 10.1098/rsta.2006.1745
* http://www.space.dtu.dk/english/Research/Scientific_data_and_models/Global_Mean_Dynamic_topography

## Software

* https://vdatum.noaa.gov/download.php

   VDatum is a free software tool being developed jointly by NOAA's National
   Geodetic Survey (NGS), Office of Coast Survey (OCS), and Center for
   Operational Oceanographic Products and Services (CO-OPS). VDatum is designed
   to vertically transform geospatial data among a variety of tidal,
   orthometric and ellipsoidal vertical datums - allowing users to convert
   their data from different horizontal/vertical references into a common
   system and enabling the fusion of diverse geospatial data in desired
   reference levels.

* https://gis.stackexchange.com/questions/11672/vertical-datum-conversion-utilities
> Question and answers about vertical datum conversion utilities


### On-line

* https://geographiclib.sourceforge.io/cgi-bin/GeoidEval?input=13.06586+-59.56153&option=Submit
* https://www.unavco.org/software/geodetic-utilities/geoid-height-calculator/geoid-height-calculator.html

### Related to GDAL

* https://github.com/OSGeo/proj.4/wiki/VerticalDatums
* https://gis.stackexchange.com/questions/73472/can-gdal-perform-vertical-transformations?noredirect=1&lq=1
* https://gis.stackexchange.com/questions/191565/vertical-datum-conversion-of-a-raster-with-gdal?rq=1
* https://trac.osgeo.org/gdal/ticket/6084
* http://www.gdal.org/gdalwarp.html
> Starting with GDAL 2.2, if the SRS has an explicit vertical datum that points
> to a PROJ.4 geoidgrids, and the input dataset is a single band dataset, a
> vertical correction will be applied to the values of the dataset.

### Vertical Datum Shift .gtx files

* http://www.gdal.org/formats_list.html
* http://download.osgeo.org/proj/vdatum/vertcon/

### On vertical datum transformations

* https://gis.stackexchange.com/questions/176689/vertical-datum-query-for-coastal-modeling?rq=1
