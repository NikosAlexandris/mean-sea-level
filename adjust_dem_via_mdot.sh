#!/bin/bash - 
#===============================================================================
#
#     COPYRIGHT: Nikos Alexandris, 2018

#       LICENSE: Licensed under the EUPL, Version 1.2 (the "Licence");
#
#                - You may not use this work except in compliance with the Licence.
#
#                - You may obtain a copy of the Licence at:
#
#                  https://joinup.ec.europa.eu/page/eupl-text-11-12
#
#                - Unless required by applicable law or agreed to inwriting, software
#                distributed under the Licence is distributed on an "AS IS" basis,
#
#                WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
#                - See the Licence for the specific language governing permissions and
#                limitations under the Licence.
#
#       date, holder and licence, and informing the reader where to find the
#       full text of the licence
#
#          FILE: adjust_dem_via_mdot.sh
#
#         USAGE: ./adjust_dem_via_mdot.sh
#
#   DESCRIPTION:
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Test: place the 'egm96_15.gtx' file under /usr/share/proj
#                and try the warping without `+geoidgrids`.
#
#        AUTHOR: Nikos Alexandris, nik@nikosalexandris.net
#  ORGANIZATION:
#       CREATED: 01/18/2018 13:38
#      REVISION:  ---
#===============================================================================



set -o nounset                              # Treat unset variables as an error

# Show if zero arguments
: ${1?"Usage: $0 DEM  EGM96GTX  MDOT  [OUTPUT_DIRECTORY]"}

# Hardcoded --------------------------------------------------------------------

# Filename suffixes

OUTPUT_DEM_SUFFIX="in_wgs84_ellipsoid.tif"
ADJUSTED_DEM_SUFFIX="adjusted.tif"
DOT='.'

# Proj Parameters

COMMON_PROJ_PARAMETERS="+proj=longlat +datum=WGS84 +nodefs"
SOURCE_PROJ_PARAMETERS="$COMMON_PROJ_PARAMETERS +geoidgrids=$EGM96_GTX"
TARGET_PROJ_PARAMETERS="$COMMON_PROJ_PARAMETERS"

# Calculation related

RESAMPLING_METHOD=bilinear
CALC_EXPRESSION='(C==0)*0 + (C!=0)*(A-B/100)'

# Globals ---------------------------------------------------------------------

OUTPUT_DIRECTORY=${4:-$DOT}

# Digital Elevation Model

INPUT_DEM=$1
INPUT_DEM_BASENAME=${INPUT_DEM##*/}
INPUT_DEM_SHORTNAME=${INPUT_DEM_BASENAME%.*}
OUTPUT_DEM="$OUTPUT_DIRECTORY/${INPUT_DEM_SHORTNAME}_${OUTPUT_DEM_SUFFIX}"
ADJUSTED_DEM="$OUTPUT_DIRECTORY/${INPUT_DEM_SHORTNAME}_${ADJUSTED_DEM_SUFFIX}"

# Vertical Datum Definition

EGM96_GTX=$2

# Mean Dynamic Ocean Topography

INPUT_MDOT=$3
INPUT_MDOT_BASENAME=${INPUT_MDOT##*/}
INPUT_MDOT_SHORTNAME=${INPUT_MDOT_BASENAME%.*}
OUTPUT_VRT="$OUTPUT_DIRECTORY/${INPUT_MDOT_SHORTNAME}_over_${INPUT_DEM_SHORTNAME}.vrt"

# Helper functions -------------------------------------------------------------

target_extent() {
    # gdalinfo dem.tif |grep Lower -A 1 -m 1 |cut -d '(' -f 2 |tr -d ',' |tr -d ')'
    gdalinfo $1 |grep Lower -A 1 -m 1 |cut -d '(' -f 2 |tr -d ',' |tr -d ')'
}

target_resolution() {
    # gdalinfo dem.tif |grep 'Pixel Size' |cut -d '(' -f2 |cut -d ',' -f1
    gdalinfo $1 |grep 'Pixel Size' |cut -d '(' -f2 |cut -d ',' -f1
}

warp() {
    gdalwarp $1 $2 -s_srs "$3" -t_srs "$4"
}

buildvrt() {
    # sample command:
    # gdalbuildvrt mean_dynamic_ocean_topography_x.vrt mean_dynamic_ocean_topography.tif -te -59.6599251 13.0355452 -59.4102029 13.3441563 -tr 0.000277777778000 0.000277777778000 -r bilinear
    gdalbuildvrt $1 $2 -te $3 $4 $5 $6 -tr $7 $8 -r $9
}

calc() {
    # sample command
    # gdal_calc.py -A dem_x.tif -B mean_dynamic_ocean_topography_x.vrt -c dem_x_egm96.tif --outfile=srtm_x_adjusted.tif --calc="a-b"
    gdal_calc.py -A $1 -B $2 -C $3 --outfile=$4 --calc="${CALC_EXPRESSION}"
}

# Main -------------------------------------------------------------------------

echo "Input parameters: $INPUT_DEM, $OUTPUT_DEM, $SOURCE_PROJ_PARAMETERS, $TARGET_PROJ_PARAMETERS"
echo

echo "Warping $INPUT_DEM"
warp $INPUT_DEM $OUTPUT_DEM "$SOURCE_PROJ_PARAMETERS" "$TARGET_PROJ_PARAMETERS"
echo

echo "Setting target extent corner coordinates"
set - $(target_extent $INPUT_DEM)
XMIN=$1 ; YMIN=$2 ; XMAX=$3 ; YMAX=$4

# unset positional parameters
set -- ...

# re-set positional parameters
set - $(target_resolution $INPUT_DEM)
XRES=$1 ; YRES=$XRES

# unset positional parameters
set -- ...
echo

echo "Building VRT for $INPUT_MDOT over $XMIN $YMIN $XMAX $YMAX"
buildvrt $OUTPUT_VRT $INPUT_MDOT $XMIN $YMIN $XMAX $YMAX $XRES $YRES $RESAMPLING_METHOD

# unset positional parameters
set -- ...
echo

echo "Calculating $ADJUSTED_DEM"
calc $OUTPUT_DEM $OUTPUT_VRT $INPUT_DEM $ADJUSTED_DEM
