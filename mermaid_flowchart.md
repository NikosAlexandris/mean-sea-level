SRTM DEM adjustment based on AVISO's Mean Dynamit Ocean Topography tidal datum


![Adjusted SRTM DEM over the Barbados island country][srtm_brb_adjusted_based_on_resampled_mdt]

[srtm_brb_adjusted_based_on_resampled_mdt]: /uploads/-/system/personal_snippet/1691922/ee0124be2aac6b535583c68bb5b68691/srtm_brb_adjusted_based_on_resampled_mdt_50percent.png "Adjusted SRTM DEM over the Barbados island country"

```mermaid
graph TD

check-reference{Referenced to WGS84 ellipsoid?}
shift-vertical-datum{Reference to WGS84 using `gdalwarp`}
crop-and-resample{Crop & Resample}
adjust-srtm-via-mdot{SRTM - MDOT}

srtm-input(Input SRTM)
srtm-egm96(SRTM WGS84/EGM96 Geoid)
srtm-wgs84(SRTM WGS84 Ellipsoid)
srtm-adjusted(Adjusted SRTM)

mdot-egm96-global("Global Mean Dynamic Ocean Topography (MDOT)")
mdot-egm96-cropped(Cropped MDOT WGS84/EGM96)
mdot-wgs84(MDOT WGS84 Ellipsoid)

srtm-input --> check-reference
check-reference -- Yes --> adjust-srtm-via-mdot
check-reference -- No --> srtm-egm96
srtm-egm96 --> shift-vertical-datum
shift-vertical-datum --> srtm-wgs84

mdot-egm96-global -.-> crop-and-resample
crop-and-resample -.-> mdot-egm96-cropped
mdot-egm96-cropped --> shift-vertical-datum
shift-vertical-datum --> mdot-wgs84

srtm-wgs84 --> adjust-srtm-via-mdot
mdot-wgs84 --> adjust-srtm-via-mdot
adjust-srtm-via-mdot ==> srtm-adjusted
```

**OR**

```mermaid
graph TD

check-reference{Referenced to WGS84 ellipsoid?}
shift-vertical-datum{Shift Vertical Datum using `gdalwarp`}
crop-and-resample{Crop & Resample}
adjust-srtm-via-mdot{SRTM - MDOT}

srtm-input(Input SRTM)
srtm-egm96(SRTM WGS84/EGM96 Geoid)
srtm-wgs84(SRTM WGS84 Ellipsoid)
srtm-adjusted(Adjusted SRTM)

mdot-egm96-global(Global Mean Dynamic Ocean Topography)
mdot-egm96-cropped(Cropped Mean Dynamic Ocean Topography WGS84/EGM96)
mdot-wgs84(Mean Dynamic Ocean Topography WGS84 Ellipsoid)

subgraph Input Data Sets
srtm-input
mdot-egm96-global
end

subgraph Reference Checks & Preprocessing
srtm-input --> check-reference
check-reference -- No --> srtm-egm96
mdot-egm96-global -.-> crop-and-resample
crop-and-resample -.-> mdot-egm96-cropped
end

subgraph Vertical Datum Shift
srtm-egm96 --> shift-vertical-datum
mdot-egm96-cropped --> shift-vertical-datum
end

subgraph Adjust SRTM
check-reference -- Yes --> srtm-wgs84
shift-vertical-datum --> srtm-wgs84
shift-vertical-datum --> mdot-wgs84
srtm-wgs84 --> adjust-srtm-via-mdot
mdot-wgs84 --> adjust-srtm-via-mdot
end

adjust-srtm-via-mdot ==> srtm-adjusted
```
